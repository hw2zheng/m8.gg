const axios = require('axios')
const API_URL = 'https://na1.api.riotgames.com/lol'

const API_LIMIT_TIME = 1200
let nextCallTime = new Date().getTime()

const queueAxiosRequest = (request, url) => {
  const now = new Date().getTime()

  if (now > nextCallTime) {
    nextCallTime = now + API_LIMIT_TIME
    return request.get(url)
  }
  const waitTime = nextCallTime - now
  nextCallTime += API_LIMIT_TIME
  return new Promise(resolve => {
    setTimeout(() => {
      resolve(request.get(url))
    }, waitTime)
  })
}

const getSummonerIds = (names) => {
  const namesPromise = []
  names.forEach(name => {
    const nameRequest = axios.create({
      headers: {
        'X-Riot-Token': process.env.RIOT_API_KEY,
        'Accept-Language': 'en-US,en;q=0.9',
        'Accept-Charset': 'application/x-www-form-urlencoded; charset=UTF-8'
      },
      baseURL: `${API_URL}`
    })
    namesPromise.push(nameRequest.get(`/summoner/v3/summoners/by-name/${encodeURIComponent(name)}`).then(response => {
      return new Promise(resolve => {
        resolve({ data: response.data, name })
      })
    }))
  })
  return namesPromise
}

const getFullMatchList = (summonerId, beginIndex, matchesAcc) => {
  const matchListRequest = axios.create({
    baseURL: `${API_URL}`,
    headers: {
      'X-Riot-Token': process.env.RIOT_API_KEY,
      'Accept-Language': 'en-US,en;q=0.9',
      'Accept-Charset': 'application/x-www-form-urlencoded; charset=UTF-8'
    },
    method: 'get',
    params: {
      beginIndex,
      season: 11
    }
  })
  return queueAxiosRequest(matchListRequest, `/match/v3/matchlists/by-account/${summonerId}`)
    .then(response => {
      const { data } = response
      beginIndex += data.matches.length
      if (beginIndex < data.totalGames && data.matches.length > 0) {
        return getFullMatchList(summonerId, beginIndex, [...matchesAcc, ...data.matches])
      } else {
        return { data: { matches: [...matchesAcc, ...data.matches] } }
      }
    })
}

const getSummonerMatches = (summonerIds) => {
  const summonerMatchesPromise = []
  summonerIds.forEach(id => {
    let matchCount = 0
    summonerMatchesPromise.push(getFullMatchList(id, 0, []))
  })
  return summonerMatchesPromise
}

const getCommonMatches = (matchLists) => {
  return matchLists[0].filter(matchToFind => {
    return matchLists.reduce((acc, matchList) => {
      if (acc) {
        const matchFound = matchList.find(match => {
          return match.gameId === matchToFind.gameId
        })
        return matchFound
      }
    }, true)
  })
}

const getMatchDetails = (matches) => {
  matches = matches.slice(0, 50) // leave this in for now
  const matchDetailsPromise = []
  matches.forEach(({ gameId }, index) => {
    const matchDetailsRequest = axios.create({
      method: 'get',
      headers: {
        'X-Riot-Token': process.env.RIOT_API_KEY,
        'Accept-Language': 'en-US,en;q=0.9',
        'Accept-Charset': 'application/x-www-form-urlencoded; charset=UTF-8'
      },
      baseURL: `${API_URL}`
    })
    matchDetailsPromise.push(queueAxiosRequest(matchDetailsRequest, `/match/v3/matches/${gameId}`))
  })
  return matchDetailsPromise
}

// Removes non solo-queue matches
const removeIrrelevantMatches = (matches) => {
  return matches.filter(match => {
    return match.queue === 420
  })
}

// Removes matches where the summoners are playing against each other
// or when matches are a remake
const filterMatchesByDetails = (matches, summoners) => {
  return matches.filter(match => {
    // Check game length
    if (match.gameDuration < 60*3.5) {
      return false
    }

    // Get list of participant identities
    const participantIds = match.participantIdentities.filter(participant => {
      return (summoners.indexOf(participant.player.accountId) !== -1)
    }).map(participant => {
      return participant.participantId
    })

    // Find the team that each participant is on
    const teamIds = match.participants.filter(participant => {
      return (participantIds.indexOf(participant.participantId) !== -1)
    }).map(participantDTO => {
      return participantDTO.teamId
    })

    for (let i = 0; i < teamIds.length - 1; i++) {
      if (teamIds[i] !== teamIds[i + 1]) {
        return false
      }
    }
    return true
  })
}

const processStats = (matchList, summoners) => {
  let teamStats = {
    totalTeamKills: 0,
    wins: 0
  }
  let summonerStats = {}
  summoners.forEach(summ => {
    summonerStats[summ] = {
      kills: 0,
      deaths: 0,
      assists: 0
    }
  })
  matchList.forEach(match => {
    const participantIdToSummoner = {}
    const participants = []
    let matchTeamId
    match.participantIdentities.forEach(participant => {
      if (summoners.indexOf(participant.player.accountId) !== -1) {
        summonerStats[participant.player.accountId].name = participant.player.summonerName
        participantIdToSummoner[participant.participantId] = participant.player.accountId
        participants.push(participant.participantId)
      }
    })

    // Set the team id
    matchTeamId = match.participants.find(participantDTO => {
      return (participants.indexOf(participantDTO.participantId) !== -1)
    }).teamId

    match.participants.forEach(participantDTO => {
      if (participants.indexOf(participantDTO.participantId) !== -1) {
        const summ = participantIdToSummoner[participantDTO.participantId]
        summonerStats[summ].kills += participantDTO.stats.kills
        summonerStats[summ].deaths += participantDTO.stats.deaths
        summonerStats[summ].assists += participantDTO.stats.assists
      }
      if (participantDTO.teamId === matchTeamId) {
        teamStats.totalTeamKills += participantDTO.stats.kills
      }
    })

    const team = match.teams.find(team => matchTeamId === team.teamId)
    if (team.win === 'Win') {
      teamStats.wins++
    }
  })

   // Convert to an array
   const summonerStatsArray = Object.keys(summonerStats).map(summonerId => summonerStats[summonerId]) 

  return { team: teamStats, summoners: summonerStatsArray, totalMatches: matchList.length }
}

const lookupSummoners = (req, res, next) => {
  console.log('RECEIVED')
  const { summoners } = req.body
  let summonerIds
  Promise.all(getSummonerIds(summoners)).then(responses => {
    summonerIds = responses.map(response => {
      const { accountId } = response.data
      return accountId
    })
    return Promise.all(getSummonerMatches(summonerIds))
  }).then(responses => {
    const matchLists = responses.map(response => response.data.matches)

    // Check all matches they have in common (doesn't verify that they're on the same team)
    let commonMatchList = getCommonMatches(matchLists)
    commonMatchList = removeIrrelevantMatches(commonMatchList)
    return Promise.all(getMatchDetails(commonMatchList))
  }).then(responses => {
    let matchDetailsList = responses.map(response => response.data)
    matchDetailsList = filterMatchesByDetails(matchDetailsList, summonerIds)
    const processedStats = processStats(matchDetailsList, summonerIds)
    console.log('FINISHED')
    res.send(processedStats)
  }).catch(err => {
    console.log(err)
  })
}

module.exports = {
  lookupSummoners
}
