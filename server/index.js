const express = require('express')
const bodyParser = require('body-parser')
const path = require('path')
const app = express()

const dotenv = require('dotenv')
dotenv.config()

// API handlers
const { lookupSummoners } = require('./summoners')

app.use(express.static(path.join(__dirname, 'build')))
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

// App Routes
app.post('/api/summoners', lookupSummoners)

app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname, 'build', 'index.html'))
})

app.listen(process.env.PORT || 3001)
