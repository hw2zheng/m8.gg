import React, { Component } from 'react'
import { MuiThemeProvider } from '@material-ui/core/styles'
import CSSBaseline from '@material-ui/core/CssBaseline'
import { Card } from '@material-ui/core'
import AppContext from './appContext'
import { Landing, Summoners } from './Pages'
import theme from './theme'
import CssBaseline from '@material-ui/core/CssBaseline';

import './theme.css'

class App extends Component {
  state = {
    data: {
      page: 1
    }
  }

  updateData (data) {
    this.setState({
      data: {
        ...this.state.data,
        ...data
      }
    })
  }

  render() {
    const { page } = this.state.data
    const PageToLoad = page === 1
      ? Landing
      : Summoners
  
    return (
      <AppContext.Provider value={{ data: this.state.data, updateData: (newData) => this.updateData(newData) }}>
        <MuiThemeProvider theme={theme}>
            <CSSBaseline />
            <PageToLoad />
        </MuiThemeProvider>
      </AppContext.Provider>
    )
  }
}

export default App
