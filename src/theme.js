import { createMuiTheme } from '@material-ui/core/styles'
import { grey, teal } from '@material-ui/core/colors'

const theme = createMuiTheme({
  palette: {
    primary: teal,
    secondary: grey,
    background: {
      paper: grey[50]
    }
  }
})
export default theme
