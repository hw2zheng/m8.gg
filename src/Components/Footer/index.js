import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import { AppBar, Button, Divider, Toolbar, Typography } from '@material-ui/core/'
import styles from './styles'

class Header extends Component {
  static propTypes = {
    classes: PropTypes.object.isRequired,
  }

  render() {
    const { classes } = this.props
    return (
    <div className={classes.root}>
      <Button>Login</Button>
      <Button>Register</Button>
      <Button>Register</Button>
      <Button>About</Button>
      </div>
    )
  }
}

export default withStyles(styles)(Header)
