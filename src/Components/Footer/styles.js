const styles = (theme) => {
  return {
    root: {
      flexGrow: 1,
      backgroundColor: theme.palette.primary[500],
      display: 'flex'
    },
    toolbar: {
      height: '100%'
    },
    menuButton: {
      marginLeft: -12,
      marginRight: 20
    }
  }
}
export default styles
