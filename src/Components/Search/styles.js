const styles = (theme) => {
  return {
    card: {
      width: 600
    },
    helpCardWrapper: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
      padding: 25,
      marginTop: 100,
      backgroundColor: 'rgba(0, 150, 136, 0.75)'
    },
    searchRoot: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      margin: 'auto',
      width: 800,
      marginBottom: 100
    },
    search: {
      backgroundColor: theme.palette.background.paper,
      padding: '0 10px',
      height: 40,
      width: '100%',
      alignItems: 'center',
      border: 'none',
      borderRadius: 5,
      textAlign: 'center',
      marginRight: 10
    },
    searchBar: {
      backgroundColor: 'rgba(0, 121, 107, 0.75)',
      padding: '10px 0',
      width: '100%'
    },
    searchHeader: {
      color: 'rgba(255, 255, 255, 0.87)',
      marginBottom: 60,
      textAlign: 'center'
    },
    searchWrapper: {
      width: '100%',
      display: 'flex',
      flexDirection: 'row'
    },
    chipStyle: {
      marginRight: 15
    },
    chipWrapper: {
      display: 'flex',
      alignItems: 'center',
      backgroundColor: theme.palette.background.paper,
      height: 50
    },
    button: {
    }
  }
}
export default styles
