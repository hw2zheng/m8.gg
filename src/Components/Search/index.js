import React, { Component } from 'react'
import axios from 'axios'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import { AppBar, Button, Chip, Input, InputAdornment, Toolbar, Typography } from '@material-ui/core/'
import { Search as SearchIcon } from '@material-ui/icons';

import AppContext from '../../appContext'
import styles from './styles'

class SearchBar extends Component {
  static propTypes = {
    classes: PropTypes.object.isRequired,
  }

  deleteText(index) {
    const names = this.state.searchText.split(',')
    names.splice(index, 1)
    this.setState({
      searchText: names.join(',')
    })
  }

  renderChips() {
    const { classes } = this.props
    const { searchText } = this.state
    const names = searchText.split(',')

    const chips = names.map((name, index) => {
      const trimName = name.trim()
      if (trimName) {
        return (<Chip
          className={classes.chipStyle}
          key={`search-chip-${index}`}
          label={trimName}
          onDelete={() => { this.deleteText(index) }}/>)
      }
      return null
    }).filter(name => {
      return name
    })

    if (chips.length > 0) {
      return chips
    } else {
      return (<Chip
        className={classes.chipStyle}
        color="inherit"
        key={`search-chip--1`}
        label={'Nothing Yet'}/>)
    }
  }

  onKeyDown (evt) {
    this.setState({
      test: true
    })
    if (evt.keyCode === 13) {
      this.props.onEnter()
    }
  }

  updateData (evt, updateDataFunction) {
    updateDataFunction({
      summonerNames: evt.target.value
    })
  }

  render() {
    const { classes } = this.props
    return (
      <AppContext.Consumer>
        { context => (
          <div>
            <div className={classes.searchRoot}>
              <Typography variant="display2" className={classes.searchHeader}>
                Enter your summoners to begin analyzing your team! 
              </Typography>
              <div className={classes.searchWrapper}>
                <Input
                  className={classes.search}
                  disableUnderline
                  onChange={(evt) => this.updateData(evt, context.updateData)}
                  inputProps={{ onKeyDown: (evt) => this.onKeyDown(evt)}}
                  placeholder="Enter summoners: summoner1, summoner2..."
                  startAdornment={
                    <InputAdornment position="start">
                      <SearchIcon />
                    </InputAdornment>
                  }
                  value={context.data.summonerNames || ''}
                />
                <Button onClick={this.props.onEnter} color="primary" variant="contained">Search</Button>
              </div>
            </div>
          </div>
        )}
      </AppContext.Consumer>
    )
  }
}

// TODO: react portal for renderchips
// USE REACT PORTAL FOR THIS
/* { searchText && 
<div className={classes.chipWrapper}>
  { this.renderChips() }
</div> } */
export default withStyles(styles)(SearchBar)
