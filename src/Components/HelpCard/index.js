import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import { Card, CardHeader, CardContent, TextField } from '@material-ui/core/'
import styles from './styles'

class HelpCard extends Component {
  static propTypes = {
    classes: PropTypes.object.isRequired,
  }

  render() {
    const { cardContent, classes, title } = this.props
    return (
        <Card className={classes.card}>
            <CardHeader
                classes={{ title: classes.title }}
                className={classes.header}
                title={title}
            />
            <CardContent>
                { cardContent }
            </CardContent>
        </Card>
    )
  }
}

export default withStyles(styles)(HelpCard)
