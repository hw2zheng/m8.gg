const styles = (theme) => {
  return {
    helpCardWrapper: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
      padding: 25,
      marginTop: 100,
      backgroundColor: 'rgba(0, 150, 136, 0.75)'
    }
  }
}
export default styles
