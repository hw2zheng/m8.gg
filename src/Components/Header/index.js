import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import { AppBar, Button, Divider, Grid, Toolbar, Typography } from '@material-ui/core/'
import styles from './styles'

class Header extends Component {
  static propTypes = {
    classes: PropTypes.object.isRequired,
  }

  render() {
    const { classes } = this.props
    return (
    <div className={classes.root}>
      <AppBar className={classes.appBar} position="static">
        <Grid container justify="center">
          <Grid item xs={6}>
            <Toolbar className={classes.toolbar}>
              <Typography variant="display1" color="inherit" className={classes.flex}>
                Mate.gg
              </Typography>
              <Button color="inherit" size="large">Home</Button>
              <Button color="inherit" >About</Button>
            </Toolbar>
          </Grid>
        </Grid>
      </AppBar>
    </div>
    )
  }
}

export default withStyles(styles)(Header)
