const styles = (theme) => {
  return {
    root: {
      flexGrow: 1
    },
    appBar: {
      backgroundColor: 'rgba(0, 150, 136, 0.75)'
    },
    toolbar: {
      height: '100%'
    },
    flex: {
      flex: 1
    },
    menuButton: {
      marginLeft: -12,
      marginRight: 20
    },
    search: {
      flexGrow: 1,
      backgroundColor: theme.palette.background.paper,
      padding: '0 10px',
      height: 40,
      alignItems: 'center',
      border: 'none'
    },
    searchBar: {
      paddingTop: 10,
      paddingBottom: 10
    },
    searchSubtext: {
      marginRight: 20
    },
    chipStyle: {
      marginRight: 15
    }
  }
}
export default styles
