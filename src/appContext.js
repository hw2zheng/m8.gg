import React from 'react'

const AppContext = React.createContext({
  data: {},
  updateData: {}
})

export default AppContext
