export default {
  landingRoot: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between'
  },
  backgroundImageWrapper: {
    height: 550,
    display: 'flex',
    flexDirection: 'column'
  },
  helpCardWrapper: {
    backgroundColor: 'rgba(0, 150, 136, 0.75)',
    borderRadius: 5,
    padding: 25,
    margin: 'auto'
  },
  spanSpacing: {
    margin: '0 5px'
  }
}
