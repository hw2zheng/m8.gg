import React, { Component } from 'react'
import axios from 'axios'
import { withStyles } from '@material-ui/core/styles'
import { Divider, Grid } from '@material-ui/core'

import AppContext from '../../appContext'
import { SummonerCard, Header, HelpCard, Footer } from '../../Components'
import styles from './styles'

const round = (number) => {
  return Math.round(number * 10) / 10
}

class Landing extends Component {
  renderSummonerDetails (data) {
    const { classes } = this.props
    const { summoners, team, totalMatches } = data
    console.log(data)
    return data.summoners.map(summoner => {
      const averageKills = round(summoner.kills / totalMatches)
      const averageDeaths = round(summoner.deaths / totalMatches)
      const averageAssists = round(summoner.assists / totalMatches)
      const killParticipation = round((summoner.kills + summoner.assists) / team.totalTeamKills * 100)
      return (
        <div>
          <Divider />
          <div>
            <span>{ summoner.name }</span>
          </div>          
          <span className={classes.spanSpacing}>IMAGE</span>
          <span className={classes.spanSpacing}>
            { averageKills }/{ averageDeaths }/{ averageAssists }
          </span>
          <span className={classes.spanSpacing}>{ killParticipation }% KP</span>
        </div>
      )
    })
  }

  renderCardContent (data) {
    return (
      <Grid container spacing={6}>
        <Grid item xs={4}>
          <div>Player Scores</div>
          { this.renderSummonerDetails(data) }
        </Grid>
        <Grid item xs={4}>
          <div>Winrate</div>
        </Grid>
        <Grid item xs={4}>
          <div>Objective Control</div>
        </Grid>
      </Grid>
    )
    return <div>TEST</div>
  }

  render () {
    const { classes } = this.props
    return (
      <AppContext.Consumer>
        { context => (
          <div className={classes.landingRoot}>
            <div>
              <Header />
            </div>
            <Grid container justify='center'
              spacing={16}
            >
              <Grid xs={6}>
                <SummonerCard title='Team Performance' cardContent={this.renderCardContent(context.data)} />
              </Grid>
            </Grid>
            <div>
              <Footer />
            </div>
          </div>
        ) }
      </AppContext.Consumer>
    )
  }
}

export default withStyles(styles)(Landing)
