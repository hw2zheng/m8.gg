import React, { Component } from 'react'
import axios from 'axios'
import { withStyles } from '@material-ui/core/styles'
import { Grid } from '@material-ui/core'

import AppContext from '../../appContext'
import { ContentCards, Header, HelpCard, Footer, Search } from '../../Components'
import styles from './styles'

class Landing extends Component {
  searchSummoners (summonerNames, updateDataFunction) {
    const summoners = summonerNames && summonerNames.split(',').filter(item => item).map(item => item.trim())
    if (summoners) {
      const options = {
        method: 'post',
        data: { summoners },
        url: '/api/summoners'
      }

      axios(options)
        .then(resp => {
          updateDataFunction({
            ...resp.data,
            page: 2
          })
        }).catch(err => {
          console.log(err)
        })
    }
  }

  render () {
    const { classes } = this.props
    return (
      <AppContext.Consumer>
        { context => (
          <div className={classes.landingRoot}>
            <div className={classes.backgroundImageWrapper}>
              <Header />
              <Search onEnter={() => this.searchSummoners(context.data.summonerNames, context.updateData)} />
            </div>
            <div>
              <Grid container justify='center'
                className={classes.helpCardWrapper}
                spacing={16}
              >
                <Grid item xs={4}>
                  <HelpCard title='See Detailed Team Statistics' cardContent='See winrate, takedowns, objective control and more! Supports Solo and Duo queue, Flex Queue, and Ranked Teams' />
                </Grid>
                <Grid item xs={4}>
                  <HelpCard title="Evaluate Your Team's Performance" cardContent='Does queueing up with your friends really help you win? Time to find out' />
                </Grid>
                <Grid item xs={4}>
                  <HelpCard title='Play to Your Strengths' cardContent='Find out which compositions work best for you!' />
                </Grid>
              </Grid>
            </div>
            <div>
              <Footer />
            </div>
          </div>
        ) }
      </AppContext.Consumer>
    )
  }
}

export default withStyles(styles)(Landing)
